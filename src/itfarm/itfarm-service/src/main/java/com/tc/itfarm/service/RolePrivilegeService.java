package com.tc.itfarm.service;

import java.sql.SQLException;
import java.util.List;

import com.tc.itfarm.model.RolePrivilege;

public interface RolePrivilegeService extends BaseService<RolePrivilege> {
	/**
	 * 根据角色id查询
	 * @param roleIds
	 * @return
	 */
	List<RolePrivilege> selectByRoleId(List<Integer> roleIds);

	/**
	 * 根据角色id查询
	 * @param roleId
	 * @return
	 */
	List<RolePrivilege> selectByRoleId(Integer roleId);

	/**
	 * 根据用户id查询
	 * @param userId
	 * @return
	 */
	List<RolePrivilege> selectByUserId(Integer userId);

	/**
	 * 插入角色权限
	 * @param id
	 * @param privilegeIds
	 * @return
	 */
	boolean insertAll(Integer id, List<Integer> privilegeIds) throws SQLException;
}
