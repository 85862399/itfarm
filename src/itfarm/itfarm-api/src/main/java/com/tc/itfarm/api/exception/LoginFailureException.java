package com.tc.itfarm.api.exception;

/**
 * Created by Administrator on 2016/8/13.
 */
public class LoginFailureException extends RuntimeException {
    public LoginFailureException() {
        super("登录失败异常");
    }

    public LoginFailureException(String message) {
        super(message);
    }

    public LoginFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
