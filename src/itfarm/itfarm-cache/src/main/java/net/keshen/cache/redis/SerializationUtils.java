/*
 * @Project: itfarm
 * @Author shenke
 * @(#)SerializationUtils.java  Created on 2016年6月22日
 * @Copyright (c) 2016 ZDSoft Inc. All rights reserved
 * @Date 2016年6月22日
 */
package net.keshen.cache.redis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;

/**
 * @description: 序列化工具类
 * @author shenke
 * @version Revision: 1.0 , 
 * @date: 2016年6月22日 下午5:25:14
 */
public class SerializationUtils {

	private static final Logger logger = LoggerFactory.getLogger(SerializationUtils.class);
	
	public static byte[] serialize(Object t) {
		if (t == null)
			return null;
		try {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			Hessian2Output out = new Hessian2Output(os);
			out.writeObject(t);
			out.flushBuffer();
			os.close();
			return os.toByteArray();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	public static Object deserialize(byte[] data) {
		if (data == null)
			return null;
		try {
			ByteArrayInputStream is = new ByteArrayInputStream(data);
			Hessian2Input in = new Hessian2Input(is);
			is.close();
			return in.readObject();
		} catch (IOException e) {
		}
		return null;
	}

}

