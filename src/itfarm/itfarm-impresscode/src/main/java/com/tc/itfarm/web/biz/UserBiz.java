package com.tc.itfarm.web.biz;

import com.google.common.collect.Lists;
import com.tc.itfarm.api.util.BeanMapper;
import com.tc.itfarm.model.Role;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.RoleService;
import com.tc.itfarm.service.UserService;
import com.tc.itfarm.web.vo.UserVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wangdongdong on 2016/8/31.
 */
@Service
public class UserBiz {
    @Resource
    private UserService userService;
    @Resource
    private RoleService roleService;

    /**
     * 获取UserVO 数据量大时放缓存
     * @param users
     * @return
     */
    public List<UserVO> getUserVOList(List<User> users) {
        List<UserVO> userVOList = Lists.newArrayList();
        for (User u : users) {
            UserVO vo = new UserVO();
            BeanMapper.copy(vo, u);
            // 获取用户所有角色
            List<Role> roles = roleService.selectByUserId(u.getRecordId());
            StringBuilder sb = new StringBuilder();
            if (roles.size() > 0) {
                for (Role r : roles) {
                    sb.append(r.getName()).append(",");
                }
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            vo.setRoles(sb.toString());
            userVOList.add(vo);
        }
        return userVOList;
    }
}
