<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:wb="http://open.weibo.com/wb">
<head>
    <title> ${config.webTitle} </title>
    <script src="${ctx}/js/template.js"></script>
    <script src="${ctx}/js/modernizr.js"></script>
    <link rel="stylesheet" href="${ctx}/css/style.css" type="text/css" media="screen"/>
    <style type="text/css">
        .navbar-default {
            background-color: white;
        }
        #banner_top {
            animation: fade-in;/*动画名称*/
            animation-duration: 1.5s;/*动画持续时间*/
            -webkit-animation:fade-in 1.5s;/*针对webkit内核*/
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#banner_top').delay(6000).slideUp();
        })
    </script>
</head>
<body>
<!-- scrollToTop -->
<!-- ================ -->
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>

<!-- page wrapper start -->
<!-- ================ -->
<div class="page-wrapper">
    <!-- header-top start -->
    <!-- ================ -->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-2 col-sm-6">

                    <!-- header-top-first start -->
                    <!-- ================ -->
                    <div class="header-top-first clearfix">
                        <a class="jiathis_button_qzone"></a>
                        <a class="jiathis_button_tsina"></a>
                        <a class="jiathis_button_tqq"></a>
                        <a class="jiathis_button_weixin"></a>
                        <a class="jiathis_button_renren"></a>
                        <a class="jiathis_button_cqq"></a>
                        <a class="jiathis_button_email"></a>
                        <a class="jiathis_button_fb"></a>
                        <a class="jiathis_button_twitter"></a>
                        <a class="jiathis_button_tianya"></a>
                        <a href="http://www.jiathis.com/share"
                           class="jiathis jiathis_txt jiathis_separator jtico jtico_jiathis" target="_blank"></a>
                        <a class="jiathis_counter_style"></a>
                    </div>
                    <!-- header-top-first end -->
                    <script type="text/javascript">
                        var jiathis_config = {
                            summary: "",
                            shortUrl: false,
                            hideMore: false
                        }
                    </script>
                    <script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>

                </div>
                <div class="col-xs-10 col-sm-6">

                    <!-- header-top-second start -->
                    <!-- ================ -->
                    <div id="header-top-second" class="clearfix">

                        <!-- header top dropdowns start -->
                        <!-- ================ -->
                        <div class="header-top-dropdown">
                            <div class="btn-group dropdown">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i
                                        class="fa fa-search"></i> Search
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                    <li>
                                        <form role="search" class="search-box">
                                            <div class="form-group has-feedback">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <i class="fa fa-search form-control-feedback"></i>
                                            </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn-group dropdown">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i
                                        class="fa fa-user"></i> Login
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                    <li>
                                        <form class="login-form">
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Username</label>
                                                <input type="text" class="form-control" placeholder="">
                                                <i class="fa fa-user form-control-feedback"></i>
                                            </div>
                                            <div class="form-group has-feedback">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" placeholder="">
                                                <i class="fa fa-lock form-control-feedback"></i>
                                            </div>
                                            <button type="submit" class="btn btn-group btn-dark btn-sm">Log In</button>
                                            <span>or</span>
                                            <button type="submit" class="btn btn-group btn-default btn-sm">Sing Up
                                            </button>
                                            <ul>
                                                <li><a href="#">Forgot your password?</a></li>
                                            </ul>
                                            <div class="divider"></div>
                                            <span class="text-center">Login with</span>
                                            <ul class="social-links clearfix">
                                                <li class="facebook"><a target="_blank" href="http://sc.chinaz.com"><i
                                                        class="fa fa-facebook"></i></a></li>
                                                <li class="twitter"><a target="_blank" href="http://sc.chinaz.com"><i
                                                        class="fa fa-twitter"></i></a></li>
                                                <li class="googleplus"><a target="_blank" href="http://sc.chinaz.com"><i
                                                        class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </form>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!--  header top dropdowns end -->

                    </div>
                    <!-- header-top-second end -->

                </div>
            </div>
        </div>
    </div>
    <!-- header-top end -->

    <!-- header start (remove fixed class from header in order to disable fixed navigation mode) -->
    <!-- ================ -->
    <header class="header fixed clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <!-- header-left start -->
                    <!-- ================ -->
                    <div class="header-left clearfix">

                        <!-- logo -->
                        <div class="logo">
                            <a href="${ctx}"><img id="logo" src="${ctx}/images/logo.png" alt="iDea"></a>
                        </div>

                        <!-- name-and-slogan -->
                        <div class="site-slogan">
                            impress &amp; code
                        </div>

                    </div>
                    <!-- header-left end -->

                </div>
                <div class="col-md-9">

                    <!-- header-right start -->
                    <!-- ================ -->
                    <div class="header-right clearfix">

                        <!-- main-navigation start -->
                        <!-- ================ -->
                        <div class="main-navigation animated">

                            <!-- navbar start -->
                            <!-- ================ -->
                            <nav class="navbar navbar-default" role="navigation">
                                <div class="container-fluid">

                                    <!-- Toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                                data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li class="dropdown active">
                                                <a href="${ctx}" class="dropdown-toggle" data-toggle="dropdown">首页</a>
                                            </li>
                                            <!-- mega-menu start -->
                                            <li class="dropdown">
                                                <a href="${ctx}/code/index.do" class="dropdown-toggle" data-toggle="dropdown">码到成功</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="${ctx}/code/index.do">JAVA</a></li>
                                                    <li><a href="${ctx}/code/index.do">CSS</a></li>
                                                    <li><a href="${ctx}/code/index.do">PHP</a></li>
                                                    <li><a href="${ctx}/code/index.do">JS</a></li>
                                                    <li><a href="${ctx}/code/index.do">HTML</a></li>
                                                </ul>
                                            </li>
                                            <!-- mega-menu end -->
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">职场人生</a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="portfolio-3col.html" class="dropdown-toggle"
                                                   data-toggle="dropdown">轻松一下</a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="shop-listing-3col.html" class="dropdown-toggle"
                                                   data-toggle="dropdown">网站定制</a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="blog-right-sidebar.html" class="dropdown-toggle"
                                                   data-toggle="dropdown">更多</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="${ctx}/admin/index.do">系统管理</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </nav>
                            <!-- navbar end -->

                        </div>
                        <!-- main-navigation end -->

                    </div>
                    <!-- header-right end -->

                </div>
            </div>
        </div>
    </header>
    <c:if test="${menuSelected == 0 && item==null}">
    <div class="banner" id="banner_top">
        <div class="section dark-translucent-bg" style="background-size: cover">
            <div class="container">
                <div class="space-top"></div>
                <h1>Welcome to iDea</h1>
                <div class="separator-2"></div>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae sit excepturi, <br class="hidden-xs hidden-sm"> hic officiis illo dolore sunt assumenda saepe id commodi sint praesentium <br class="hidden-xs hidden-sm"> natus laborum quas cumque facilis, suscipit aliquam dolorum.</p>
            </div>
        </div>
    </div>
    </c:if>
</div>
</body>
</html>