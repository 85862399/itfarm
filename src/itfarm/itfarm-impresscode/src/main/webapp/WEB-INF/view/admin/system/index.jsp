<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
    <jsp:include page="../header.jsp"></jsp:include>
    <div id="dcMain">
        <!-- 当前位置 -->
        <div id="urHere">DouPHP 管理中心<b></b><strong>系统设置</strong></div>
        <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
            <h3>系统设置</h3>
            <script type="text/javascript">

                $(function () {
                    $(".idTabs").idTabs();
                });

            </script>
            <div class="idTabs">
                <ul class="tab">
                    <li><a href="#main">常规设置</a></li>
                    <li><a href="#display">显示设置</a></li>
                    <li><a href="#defined">自定义</a></li>
                    <li><a href="#mail">邮件服务器</a></li>
                </ul>
                <div class="items">
                    <form action="${ctx}/system/save.do" method="post" >
                        <input type="hidden" name="recordId" value="${config.recordId}">
                        <div id="main">
                            <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                                <tr>
                                    <th width="131">名称</th>
                                    <th>内容</th>
                                </tr>
                                <tr>
                                    <td align="right">web站点名称</td>
                                    <td>
                                        <input type="text" name="webName" value="${config.webName}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">web站点标题</td>
                                    <td>
                                        <input type="text" name="webTitle" value="${config.webTitle}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">web访问地址</td>
                                    <td>
                                        <input type="text" name="webUrl" value="${config.webUrl}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">admin站点名称</td>
                                    <td>
                                        <input type="text" name="adminName" value="${config.adminName}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">admin站点标题</td>
                                    <td>
                                        <input type="text" name="adminTitle" value="${config.adminTitle}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">admin访问地址</td>
                                    <td>
                                        <input type="text" name="adminUrl" value="${config.adminUrl}" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">系统语言</td>
                                    <td>
                                        <select name="language">
                                            <option value="en_us">en_us</option>
                                            <option value="zh_cn" selected>zh_cn</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="display">
                            <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                                <tr>
                                    <th width="131">名称</th>
                                    <th>内容</th>
                                </tr>
                                <tr>
                                    <td align="right">缩略图宽度</td>
                                    <td>
                                        <input type="text" name="thumb_width" value="135" size="80" class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">缩略图高度</td>
                                    <td>
                                        <input type="text" name="thumb_height" value="135" size="80" class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">商品价格保留小数位数</td>
                                    <td>
                                        <input type="text" name="price_decimal" value="2" size="80" class="inpMain"/>
                                        <p class="cue">将以四舍五入形式保留小数</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">文章列表数量</td>
                                    <td>
                                        <input type="text" name="display[article]" value="10" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">首页展示文章数量</td>
                                    <td>
                                        <input type="text" name="display[home_article]" value="5" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">商品列表数量</td>
                                    <td>
                                        <input type="text" name="display[product]" value="10" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">首页展示商品数量</td>
                                    <td>
                                        <input type="text" name="display[home_product]" value="4" size="80"
                                               class="inpMain"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="defined">
                            <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                                <tr>
                                    <th width="131">名称</th>
                                    <th>内容</th>
                                </tr>
                                <tr>
                                    <td align="right">文章自定义属性</td>
                                    <td>
                                        <input type="text" name="defined[article]" value="" size="80" class="inpMain"/>
                                        <p class="cue">如"颜色,尺寸,型号"中间以英文逗号隔开</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">商品自定义属性</td>
                                    <td>
                                        <input type="text" name="defined[product]" value="" size="80" class="inpMain"/>
                                        <p class="cue">如"颜色,尺寸,型号"中间以英文逗号隔开</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="mail">
                            <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                                <tr>
                                    <th width="131">名称</th>
                                    <th>内容</th>
                                </tr>
                                <tr>
                                    <td align="right">邮件服务</td>
                                    <td>
                                        <label for="mail_service_0">
                                            <input type="radio" name="mail_service" id="mail_service_0" value="0"
                                                   checked="true">
                                            系统内置Mail服务</label>
                                        <label for="mail_service_1">
                                            <input type="radio" name="mail_service" id="mail_service_1" value="1">
                                            SMTP服务</label>
                                        <span class="cue ml">如果选择系统内置Mail服务则以下SMTP有关信息无需填写</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">SMTP服务器</td>
                                    <td>
                                        <input type="text" name="mail_host" value="smtp.domain.com" size="80"
                                               class="inpMain"/>
                                        <p class="cue">一般邮件服务器地址为：smtp.domain.com，如果是本机则对应localhost即可</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">服务器端口</td>
                                    <td>
                                        <input type="text" name="mail_port" value="25" size="80" class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">是否使用SSL安全协议</td>
                                    <td>
                                        <label for="mail_ssl_0">
                                            <input type="radio" name="mail_ssl" id="mail_ssl_0" value="0"
                                                   checked="true">
                                            否</label>
                                        <label for="mail_ssl_1">
                                            <input type="radio" name="mail_ssl" id="mail_ssl_1" value="1">
                                            是</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">发件邮箱</td>
                                    <td>
                                        <input type="text" name="mail_username" value="" size="80" class="inpMain"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">发件邮箱密码</td>
                                    <td>
                                        <input type="text" name="mail_password" value="" size="80" class="inpMain"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" border="0" cellpadding="8" cellspacing="0" class="tableBasic">
                            <tr>
                                <td width="131"></td>
                                <td>
                                    <input type="hidden" name="token" value="24760807"/>
                                    <input name="submit" class="btn" type="submit" value="提交"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
    <div class="clear"></div>
</div>
</body>
</html>